db.users.drop();
db.appointments.drop();
let res = db.users.insertMany([
  {
    fullname: 'Зубенко Михаил Петрович1',
    phone: '09123456789',
    password: '$2a$10$I2UKcWRr4V4R561unV9EIeL2pUL0BLdc/lWvAKaTeLPFoA8duWouS',
    type: 'client',
  },
  {
    fullname: 'Зубенко Михаил Петрович2',
    phone: '09123456788',
    password: '$2a$10$I2UKcWRr4V4R561unV9EIeL2pUL0BLdc/lWvAKaTeLPFoA8duWouS',
    type: 'client',
  },
  {
    fullname: 'Зубенко Михаил Петрович3',
    phone: '09123456787',
    password: '$2a$10$I2UKcWRr4V4R561unV9EIeL2pUL0BLdc/lWvAKaTeLPFoA8duWouS',
    type: 'lawyer',
    field: ['administrative', 'civil', 'criminal', 'family'],
  },

  {
    fullname: 'Зубенко Михаил Петрович4',
    phone: '09123456786',
    password: '$2a$10$I2UKcWRr4V4R561unV9EIeL2pUL0BLdc/lWvAKaTeLPFoA8duWouS',
    type: 'lawyer',
    field: ['criminal'],
  },
]);
