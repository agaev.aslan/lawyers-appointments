export default () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  mongo: {
    host: process.env.MONGO_HOST || 'host.docker.internal',
    port: process.env.MONGO_PORT || 27017,
    db: process.env.MONGO_DB || 'iwow',
    pass: process.env.MONGO_PASS || 'iwow',
    user: process.env.MONGO_USER || 'iwow',
  },
  jwtSecret: process.env.JWT_SECRET || 'iwow',
});
