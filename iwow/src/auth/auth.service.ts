import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { hash, compare } from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
  ) {}

  async hashPassword(password: string): Promise<string> {
    return hash(password, 10);
  }

  async validateUser(phone: string, pass: string): Promise<any> {
    const { password, ...user } = await this.userService.get(phone);

    return await compare(pass, password) ? user : null;
  }

  async login(user: any) {
    const payload = { username: user.phone, sub: user._id };
    return {
      access_token: this.jwtService.sign(payload, { secret: 'iwow' }),
    };
  }
}
