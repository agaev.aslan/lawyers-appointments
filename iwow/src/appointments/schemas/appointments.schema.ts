import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type AppointmentDocument = HydratedDocument<Appointment>;

@Schema()
export class Appointment {
  @Prop({ required: true })
  clientId: string;

  @Prop({ required: true })
  lawyerId: string;

  @Prop({ required: true })
  time: string;
}

export const AppointmentSchema = SchemaFactory.createForClass(Appointment);
