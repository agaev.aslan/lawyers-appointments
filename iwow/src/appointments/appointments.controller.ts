import { Controller, Post, UseGuards, Request } from '@nestjs/common';
import { JwtGuard } from 'src/auth/guards/jwt.guard';
import { AppointmentsService } from './appointments.service';

@Controller('appointments')
export class AppointmentsController {
  constructor(private appointmentsService: AppointmentsService) {}

  @UseGuards(JwtGuard)
  @Post()
  async foo(@Request() req) {
    const appointment = {
      clientId: req.user.userId,
      field: req.body.field,
      time: req.body.time,
    };

    try {
      const createdAppointment = await this.appointmentsService.create(
        appointment,
        req.user.phone,
      );

      return createdAppointment;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
