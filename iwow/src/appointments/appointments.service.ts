import { appendFile } from 'fs/promises';
import { join } from 'path';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SchedulerRegistry } from '@nestjs/schedule';
import { CronJob } from 'cron';
import { Model } from 'mongoose';
import { UsersService } from 'src/users/users.service';
import { CreateAppointmentDto } from './dto/createAppointment.dto';
import {
  Appointment,
  AppointmentDocument,
} from './schemas/appointments.schema';

const APPOINTMENT_DURATION_MIN = 40;

@Injectable()
export class AppointmentsService {
  constructor(
    @InjectModel(Appointment.name)
    private readonly appointmentModel: Model<AppointmentDocument>,
    private userService: UsersService,
    private schedulerRegistry: SchedulerRegistry,
  ) {}

  async create(appointment: CreateAppointmentDto, clientPhone) {
    const lawyers = await this.userService.getLawyersByField(appointment.field);

    // uncomment if needed for testing
    // await this.appointmentModel.deleteMany();

    // query and group appointments by lawyers
    const appointmentsByLawyers = await this.appointmentModel.aggregate([
      {
        $match: {
          lawyerId: { $in: lawyers.map((lawyer) => lawyer._id.toString()) },
        },
      },
      {
        $group: {
          _id: '$lawyerId',
          appointments: { $push: '$$ROOT' },
        },
      },
    ]);

    // create a map for faster access
    const appointmentsByLawyersMap = appointmentsByLawyers.reduce(
      (acc, lawyer) => {
        acc[lawyer._id] = lawyer.appointments;
        return acc;
      },
      {},
    );

    // find lawyer with no overlapping appointments
    const lawyer = lawyers.find((lawyer) => {
      const lawyerAppointments =
        appointmentsByLawyersMap[lawyer._id.toString()] || [];

      return !lawyerAppointments.some((lawyerAppointment) => {
        const appointmentTime = new Date(appointment.time);
        const appointmentEndTime = new Date(
          appointmentTime.getTime() + APPOINTMENT_DURATION_MIN * 60000,
        );
        const lawyerAppointmentTime = new Date(lawyerAppointment.time);
        const lawyerAppointmentEndTime = new Date(
          lawyerAppointmentTime.getTime() + APPOINTMENT_DURATION_MIN * 60000,
        );
        return (
          (appointmentTime >= lawyerAppointmentTime &&
            appointmentTime <= lawyerAppointmentEndTime) ||
          (appointmentEndTime >= lawyerAppointmentTime &&
            appointmentEndTime <= lawyerAppointmentEndTime)
        );
      });
    });

    if (!lawyer) {
      throw new HttpException('No lawyers available', HttpStatus.NOT_FOUND);
    }

    const client = await this.userService.get(clientPhone);

    const createdAppointments = await this.appointmentModel.insertMany([
      {
        ...appointment,
        lawyerId: lawyer._id,
        time: new Date(appointment.time),
      },
    ]);

    if (!createdAppointments.length) {
      return null;
    }

    const createdAppointment = createdAppointments[0];
    this.addReminders(createdAppointment, lawyer.fullname, client.fullname);

    return createdAppointments[0];
  }

  addReminders(
    appointment: AppointmentDocument,
    lawyerName: string,
    clientName: string,
  ) {
    const appointmentTime = new Date(appointment.time);
    const twoHoursBefore = new Date(appointmentTime.getTime() - 2 * 60 * 60000);

    const cronTimeTwoHours = `${twoHoursBefore.getMinutes()} ${twoHoursBefore.getHours()} ${twoHoursBefore.getDate()} ${
      twoHoursBefore.getMonth() + 1
    } *`;
    const msgTwoHours = `\nПривет, ${clientName}. Через 2 часа у вас консультация с юристом ${lawyerName}.`;
    this.addCronReminder(
      cronTimeTwoHours,
      msgTwoHours,
      appointment._id.toString(),
    );

    const oneDayBefore = new Date(appointmentTime.getTime() - 24 * 60 * 60000);
    const cronTimeOneDay = `${oneDayBefore.getMinutes()} ${oneDayBefore.getHours()} ${oneDayBefore.getDate()} ${
      oneDayBefore.getMonth() + 1
    } *`;
    const msgOneDay = `\nПривет, ${clientName}. Напоминаем о консультации с юристом ${lawyerName} завтра в ${appointmentTime.getHours()}:${appointmentTime.getMinutes()}.`;

    this.addCronReminder(cronTimeOneDay, msgOneDay, appointment._id.toString());
  }

  async addCronReminder(cronTime: string, message: string, id: string) {
    // leaving this for testing
    const filePath = join(__dirname, '../../public/reminders.log');
    await appendFile(filePath, message);

    const job = new CronJob(cronTime, async () => {
      const filePath = join(__dirname, '../../public/reminders.log');
      await appendFile(filePath, message);

      job.stop();
    });

    this.schedulerRegistry.addCronJob(`${id}-${cronTime}-reminder`, job);
    job.start();
  }
}
