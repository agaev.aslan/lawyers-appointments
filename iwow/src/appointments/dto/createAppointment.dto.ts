export type CreateAppointmentDto = {
  clientId: string;
  field: string;
  time: string;
};
