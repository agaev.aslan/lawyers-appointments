import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './schemas/users.schema';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
  ) {}

  async get(phone: string) {
    const user = await this.userModel.findOne({ phone }).lean();
    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }

    return user;
  }

  async getLawyersByField(field: string) {
    const lawyers = await this.userModel.find({ field });

    if (!lawyers?.length) {
      throw new HttpException('Lawyer not found', HttpStatus.NOT_FOUND);
    }

    return lawyers;
  }
}
