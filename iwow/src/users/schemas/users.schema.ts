import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type UserDocument = HydratedDocument<User>;

@Schema()
export class User {
  @Prop({ required: true })
  fullname: string;

  @Prop({ required: true, unique: true })
  phone: string;

  @Prop({ required: true })
  password: string;

  @Prop({
    type: String,
    enum: ['lawyer', 'client'],
    default: 'client',
  })
  type: string;

  @Prop({
    type: [String],
    enum: [
      'administrative',
      'civil',
      'criminal',
      'family',
      'labor',
      'tax',
      'other',
    ],
  })
  field: string[];
}

export const UserSchema = SchemaFactory.createForClass(User);
