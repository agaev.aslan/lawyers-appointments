Mock data is added in `mongo/mongo-init.js`.

Env variables are present in this repo for easier deployment, they can be treated as template values.

**Usage**

Start project using `docker-compose up --build`.

Log in <br>

> curl localhost:3003/auth/login -X POST -d '{"phone":"09123456789","password":"123456"}' -H "Content-Type: application/json"

Use access_token from the response for next request:

> curl localhost:3003/appointments -X POST -H "Authorization: Bearer YOUR ACCESS TOKEN HERE" -d '{"time": "2023-01-02:12:12", "field": "criminal"}' -H "Content-Type: application/json"

`field` can be one of [
      'administrative',
      'civil',
      'criminal',
      'family',
      'labor',
      'tax',
      'other'
    ]


If there are no suitable lawyers with no overlapping appointments, new appointment will not be created.
Reminders will be written to `iwow/public/reminders.log`.